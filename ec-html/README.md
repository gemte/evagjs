ec-html
==

evag.io API interaction library using HTML components.

## Getting started

Install using npm (or yarn or anything):

```
$ npm install @evag/ec-html
```

Create a HTML form with `data-evag` attributes:

```html
<div data-evag="multiforms">
  <form action="https://api.evag.io/channels/xxx/submit" method="post" data-evag="form">
    <input type="text" name="name" placeholder="Name" />
    <input type="email" name="email" placeholder="E-mail" />
    <input type="tel" name="phone" placeholder="Phone" data-evag="phone" />
    <select name="uf">
      <option disabled="disabled" selected="selected">Select UF...</option>
      <option value="AC">AC</option>
      ...
      <option value="TO">TO</option>
    </select>
    <select name="city">
      <!-- The script will automatically load cities on UF change. -->
    </select>
    <button type="submit">Submit</button>
  </form>

  <div data-evag="form">
    <p>Thank you for submitting the form!</p>
  </div>
</div>
```

Use `@evag/ec-html` to handle forms:

```js
import ec from '@evag/ec-html';

window.addEventListener('load', () => {
  document.querySelectorAll('[data-evag="form"]').forEach(ec.forms.setup);
});
```

Read `src/forms.js` JSDoc for more information.
