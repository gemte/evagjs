import {captchaV3Key} from './core';

let initialized = false;

const setup = () => {
  if (window.grecaptcha || initialized) {
    return;
  }

  window.ECRecaptchaCallback = () => {
    window.grecaptcha.ready(() => {
      window.grecaptcha.execute(captchaV3Key, {action: 'homepage'});
    });
  };

  initialized = true;

  const el = document.createElement('script');
  el.async = true;
  el.src = `https://www.google.com/recaptcha/api.js?onload=ECRecaptchaCallback&render=${captchaV3Key}`;

  const first = document.getElementsByTagName('script')[0];
  first.parentNode.insertBefore(el, first);
};

export default {setup};
