const cls = 'evag-snackbar';
const displayTime = 5000;

export const displayMessage = function (text) {
  const body = document.getElementsByTagName('body')[0];
  const element = document.createElement('div');
  element.className = cls;
  element.style.maxWidth = Math.min(body.offsetWidth - 30, 400) + 'px';
  element.innerHTML = text;

  body.appendChild(element);
  window.setTimeout(() => {
    element.classList.add(`${cls}-shown`);
  }, 0);
  window.setTimeout(() => {
    element.classList.remove(`${cls}-shown`);
  }, displayTime);
  window.setTimeout(() => {
    body.removeChild(element);
  }, displayTime + 1000);
};
