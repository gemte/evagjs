# ec-react

EVAG API interaction library with React components.


# Install

    yarn add @evag/ec-react
    
# Usage

    import {Form} from '@evag/ec-react';
    // Inputs and modal error style
    import '@evag/ec-react/dist/main.css';

# Setup for local package link

If you just do a `yarn link:{path_to_this_folder}`, the React installed in this node_modules will conflict with the one installed in the other project. So you need:

    cd {this-folder}
    yarn link
    yarn install
    cd node_modules/react
    yarn link
    cd {gatsby-project}
    yarn link @evag/ec-react
    yarn link react
