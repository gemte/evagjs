import {cepData} from './core';

export const handleCEPChange = (
  setDisabledLogradouro,
  setFieldValue,
  refNumero,
  refLogradouro,
  onChange
) => {
  return async (event) => {
    event.persist();
    const numeric = event.target.value.replace(/[^0-9]/, '');
    if (numeric.length === 8) {
      setDisabledLogradouro && setDisabledLogradouro(true);

      let focus = true;
      try {
        const data = await cepData(numeric);
        if (data.endLogradouro && setFieldValue) {
          setFieldValue('endLogradouro', data.endLogradouro);
          setFieldValue('district', data.district);
          if (refNumero) {
            refNumero.current.focus();
            focus = false;
          }
        }
      } catch (error) {
        console.error(error);
      }

      setDisabledLogradouro && setDisabledLogradouro(false);
      if (focus && refLogradouro) {
        refLogradouro.current.focus();
      }
    }

    onChange && onChange(event);
  };
};
