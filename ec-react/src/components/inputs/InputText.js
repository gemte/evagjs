import React from 'react';
import PropTypes from 'prop-types';
import {ErrorMessage} from 'formik';
import Cleave from 'cleave.js/react';

import s from './input.module.scss';

class ErrorBoundary extends React.Component {
  constructor({props}) {
    super(props);
    this.state = {hasError: false};
  }

  static getDerivedStateFromError(error) {
    return {hasError: true};
  }

  componentDidCatch(error, errorInfo) {
    console.log(
      'InputText Error, using fallback without Cleave',
      error,
      errorInfo
    );
  }

  render() {
    if (this.state.hasError) {
      return this.props.fallback;
    }
    return this.props.children;
  }
}

const InputText = React.forwardRef(
  ({className, cleave, type, ...props}, ref) => {
    let element = (
      <input className={className} ref={ref} type={type} {...props} />
    );
    if (type !== 'text') {
      element = (
        <ErrorBoundary fallback={element}>
          <Cleave
            className={className}
            options={cleave}
            htmlRef={(r) => ref && (ref.current = r)}
            type={type}
            {...props}
          />
        </ErrorBoundary>
      );
    }

    return (
      <div
        style={{
          display: 'inline-block',
          position: 'relative',
          width: props.width,
        }}
      >
        {element}
        <ErrorMessage name={props.name} component="div" className={s.error} />
      </div>
    );
  }
);

InputText.defaultProps = {
  className: '',
  cleave: {},
  type: 'text',
  width: '100%',
};

InputText.propTypes = {
  className: PropTypes.string,
  cleave: PropTypes.object,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  width: PropTypes.string,
};

export default InputText;
