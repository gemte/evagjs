import React from 'react';
import PropTypes from 'prop-types';
import {Formik, Form} from 'formik';
import { BallTriangle } from 'react-loader-spinner';
import Recaptcha from 'react-recaptcha';

import {submit, captchaV2Key, baseURL, logErr} from '../core';
import ModalError from './ModalError';
import {trackConversion} from '../../../core/tracking';

const handleFormError = (error, setFieldError, setFormError) => {
  var errorMsg;
  if (error.response && typeof error.response.data === 'object') {
    for (let name in error.response.data) {
      if (!error.response.data.hasOwnProperty(name)) {
        continue;
      }
      setFieldError(name, error.response.data[name]);
    }
    return;
  } else if (error.response) {
    // Server returned error.
    errorMsg = error.response;
  } else if (error.request) {
    // No response received.
    errorMsg = error.request;
  } else {
    // Something triggered an error when setting the request.
    errorMsg = error.message;
  }

  let extra;
  if (typeof errorMsg === 'object') {
    extra = errorMsg;
    errorMsg = errorMsg.data;
  }
  logErr(error, 'handleFormError: ' + errorMsg, extra);

  setFormError(
    'Ocorreu um erro inesperado ao enviar o formulário. Tente novamente mais tarde.'
  );
};

const EvagForm = ({forms}) => {
  const [current, setCurr] = React.useState({pos: 0, carryValues: {}});
  const [formError, setFormError] = React.useState(null);
  const [verifyCallback, setVerifyCallback] = React.useState(null);
  const currForm = forms[current.pos];

  if (typeof currForm === 'string') {
    return <p>{currForm}</p>;
  }

  const dismiss = () => {
    setFormError(null);
  };

  const onSuccess = currForm.onSuccess
    ? currForm.onSuccess
    : ({jump}) => jump(1);

  const jump = (steps, values = {}) => {
    setCurr({
      pos: Math.min(current.pos + steps, forms.length - 1),
      carryValues: values,
    });
  };

  const handleSubmits = async (values, {setFieldError, setSubmitting}) => {
    try {
      await submit(currForm.channelId, values, currForm.captcha ? 'v3' : null);
      onSuccess({jump, values});
      trackConversion(currForm.channelId);
    } catch (error) {
      if (error.response.status === 401) {
        setVerifyCallback(() => async (token) => {
          try {
            await submit(currForm.channelId, values, 'v2', token);
            onSuccess({jump, values});
          } catch (error_) {
            handleFormError(error_, setFieldError, setFormError);
          }
          setVerifyCallback(null);
        });
      } else {
        handleFormError(error, setFieldError, setFormError);
      }
      setSubmitting(false);
    }
  };

  return (
    <Formik
      key={current.pos} // this forces Formik component recreation on form jump
      initialValues={{...currForm.initialValues, ...current.carryValues}}
      onSubmit={handleSubmits}
      validationSchema={currForm.validationSchema}
    >
      {({values, handleBlur, handleChange, isSubmitting, setFieldValue}) => {
        if (isSubmitting) {
          return (
            <BallTriangle
              color="#FFFFFF"
              height={50}
              width={50}
            />
          );
        }

        if (verifyCallback) {
          return (
            <>
              <p>
                Por favor, confirme que você não é um robô para validar o envio
                do formulário.
              </p>
              <Recaptcha
                sitekey={captchaV2Key}
                verifyCallback={verifyCallback}
              />
            </>
          );
        }

        return (
          <>
            <Form
              action={`${baseURL}${currForm.channelId}/submit`}
              method="post"
              className={currForm.className}
            >
              {React.createElement(currForm.element, {
                onChange: handleChange,
                onBlur: handleBlur,
                setFieldValue,
                values,
                ...currForm,
              })}
            </Form>
            <ModalError error={formError} dismiss={dismiss} />
          </>
        );
      }}
    </Formik>
  );
};

EvagForm.displayName = 'EvagForm';

EvagForm.propTypes = {
  forms: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.shape({
        captcha: PropTypes.string,
        className: PropTypes.string,
        channelId: PropTypes.string.isRequired,
        element: PropTypes.func.isRequired,
        initialValues: PropTypes.string,
        props: PropTypes.object,
        onSuccess: PropTypes.func,
        validationSchema: PropTypes.string,
      }),
    ])
  ).isRequired,
};

export default EvagForm;
