import * as Sentry from '@sentry/browser';

import {api} from './api';
import {checkPromise} from './logs';
import {captchaV3Key} from './captcha';
import {trackFacebookSubmission, trackGoogleSubmission, trackSubmission} from './tracking';

// Gets a channel count.
export const count = async (channelId) => {
  return await checkPromise(
    api.get(`/channels/${channelId}/count`),
    'Cant get channel count'
  );
};

// Gets find data about a channel.
export const find = async (channelId) => {
  return await checkPromise(
    api.get(`/channels/${channelId}/find`),
    'Cant get channel find'
  );
};

// Submits data to a channel. Tries to handle captcha.
export const submit = async (channelId, values, captchaMode, captchaVal,
                             fbEventName, gaEventName, gAds) => {
  const common = {};
  if ('evag' in window) {
    if ('getEvlt' in window.evag) {
      common.evlt = window.evag.getEvlt();
    }

    if ('getProjectID' in window.evag) {
      common.projectID = window.evag.getProjectID();
    }
  }

  const data = Object.assign({}, common, values);
  delete data.redirect;

  if (captchaMode) {
    if (!window.grecaptcha) {
      const error = new Error('ReCaptcha lib not loaded');
      Sentry.captureException(error);
      throw error;
    }

    data.captchaMode = captchaMode;
    data.captchaVal = captchaVal;

    if (captchaMode === 'v3' && !captchaVal) {
      try {
        data.captchaVal = await checkPromise(
          window.grecaptcha.execute(captchaV3Key, {
            action: 'submit',
          }),
          'ReCaptcha execute error'
        );
      } catch (_) {}
    }
  }

  trackSubmission(channelId, fbEventName, gaEventName, document.title);

  return await api.post(`/channels/${channelId}/submit`, data);
};
