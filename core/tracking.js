export const trackFacebookSubmission = (channelId, customName, page) => {
  if (window.fbq) {
    window.fbq('track', 'Lead', {content_name: channelId});
    window.fbq('trackCustom', `[Submissão] ${channelId}`);

    if (!!!customName) {
      window.fbq('track', 'Lead', {content_name: customName});
      window.fbq('trackCustom', `[Submissão] ${customName}`);
    }

    window.fbq('trackCustom', `[Submissão] [Page] "${page}"`);
  }
};

export const trackGoogleSubmission = (channelId, customName, page) => {
  if (window.gtag) {
    // https://developers.google.com/analytics/devguides/collection/gtagjs/events
    window.gtag('event', 'submit', {
      event_category: 'Form',
      event_label: channelId,
    });
    window.gtag('event', `[Submissão] ${channelId.replaceAll('-', '_')}`);
    if (!!!customName) {
      window.gtag('event', 'submit', {
        event_category: 'Form',
        event_label: customName,
      });
      window.gtag('event', `[Submissão] ${customName}`);
    }
    window.gtag('event', `[Submissão] [Page] "${page}"`);
  }

  if (window.ga) {
    // https://developers.google.com/analytics/devguides/collection/analyticsjs/sending-hits
    window.ga('send', 'event', 'Form', 'submit', channelId);
    if (!!!customName) {
      window.ga('send', 'event', 'Form', 'submit', customName);
    }
  }
};

// Send button click of form submission events to marketing tools.
export const trackSubmission = (channelId, customFbName, customGoogleName, page) => {
  trackFacebookSubmission(channelId, customFbName, page);
  trackGoogleSubmission(channelId, customGoogleName, page);
};

export const trackFacebookConversion = (channelId, customName, page) => {
  if (window.fbq) {
    window.fbq('track', 'CompleteRegistration', {content_name: channelId});
    window.fbq('trackCustom', `[Conversão] ${channelId}`);

    if (!!!customName) {
      window.fbq('track', 'CompleteRegistration', {content_name: customName});
      window.fbq('trackCustom', `[Conversão] ${customName}`);
    }

    window.fbq('trackCustom', `[Conversão] [Page] "${page}"`);
  }
};

export const trackGoogleConversion = (channelId, customName, page, gAdsConversion) => {
  if (window.gtag) {
    // https://developers.google.com/analytics/devguides/collection/gtagjs/events
    window.gtag('event', 'conversao', {
      event_category: 'Campaign',
      event_label: channelId,
    });
    window.gtag('event', `[Conversão] ${channelId.replaceAll('-', '_')}`);

    if (!!!customName) {
      window.gtag('event', 'conversao', {
        event_category: 'Campaign',
        event_label: customName,
      });
      window.gtag('event', `[Conversão] ${customName}`);
    }

    window.gtag('event', `[Conversão] [Page] "${page}"`);

    let options = null;
    if (gAdsConversion) {
      options = {
        send_to: `${gAdsConversion.id}/${gAdsConversion.label}`,
      }

      window.gtag('event', `conversion`, options);
    }
  }

  if (window.ga) {
    // https://developers.google.com/analytics/devguides/collection/analyticsjs/sending-hits
    window.ga('send', 'event', 'conversao', 'lead', channelId);
    if (!!!customName) {
      window.ga('send', 'event', 'conversao', 'lead', customName);
    }
  }
};

// Send valid form submission events to marketing tools.
export const trackConversion = (channelId, customFbName, customGoogleName, page, gAdsConversion) => {
  trackFacebookConversion(channelId, customFbName, page);
  trackGoogleConversion(channelId, customGoogleName, page, gAdsConversion);
};
