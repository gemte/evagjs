import * as Sentry from '@sentry/browser';
import * as config from '../../package.json';

if (typeof window !== 'undefined') {
  Sentry.init({
    dsn: 'https://48fac91a62aa46ac8de4b43cda035d77@sentry.evag.dev/5',
    environment: window.location.port ? 'dev' : 'prod',
    release: `${config.name.replace('/', ':')}:${config.version}`,
  });
}

// Logs an error to Sentry and console enriching it with `name`.
export const logErr = async (error, name, extra, toConsole = true) => {
  if (!error) {
    error = new Error('logErr received null error');
  }

  const prevName = error.name;
  error.name = name || error.name;

  Sentry.withScope(function (scope) {
    scope.setExtra('errorName', prevName);
    extra && scope.setExtra('extra', extra);
    const c = error.config;
    if (c) {
      c.method && scope.setExtra('method', error.config.method);
      c.baseURL && scope.setExtra('requestBaseUrl', error.config.baseURL);
      c.url && scope.setExtra('requestUrl', error.config.url);
    }
    Sentry.captureException(error);
  });

  error.name = prevName;

  if (toConsole) {
    if (name) {
      console.error(name, error);
    } else {
      console.error(error);
    }
  }
};

// Receives a promise and forwards it. On exception, logs to Sentry and console
// enriching it with `name`, then throws the unchanged intial exception.
export const checkPromise = async (promise, name, extra, toConsole = true) => {
  try {
    return await promise;
  } catch (error) {
    logErr(error, name, extra, toConsole);
    throw error;
  }
};
