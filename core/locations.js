import axios from 'axios';
import {api} from './api';
import {checkPromise} from './logs';

let locationsCache = {};

// Returns data about a CEP.
export const cepData = async (numeric) => {
  try {
    const response = await checkPromise(
      axios.get(`https://viacep.com.br/ws/${numeric}/json/`),
      'Cant get CEP data'
    );
    if (response.data) {
      return {
        uf: response.data.uf,
        city: response.data.localidade,
        district: response.data.bairro,
        endLogradouro: response.data.logradouro,
        endComplemento: response.data.complemento,
      };
    }
  } catch (_) {}
  return {};
};

// Returns a list of locations. Uses a cache.
export const locations = async (country = null, state = null, city = null) => {
  let uri = '/locations';
  if (country) {
    uri += `/${country}`;
    if (state && state !== 'UF') {
      uri += `/${state}`;
      if (city) {
        uri += `/${city}`;
      }
    }
  }

  if (!locationsCache[uri]) {
    try {
      locationsCache[uri] = (
        await checkPromise(api.get(uri), 'Cant get locations')
      ).data;
    } catch (_) {}
  }

  return locationsCache[uri] || [];
};
