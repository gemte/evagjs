import axios from 'axios';
import axiosRetry from 'axios-retry';

export const baseURL = 'https://api.evag.io/';

// Configure an axios client with default values.
export const configClient = (client) => {
  axiosRetry(client, {
    retries: 1000,
    retryDelay: axiosRetry.exponentialDelay,
  });
  return client;
};

// Axios instance prepared to access EVAG API.
export const api = configClient(axios.create({baseURL, withCredentials: true}));
