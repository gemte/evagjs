evagjs
==

This repository contains code to integrate websites to evag.io API.

It provides two JavaScript/NPM libraries:

- **ec-html:** scripts for sites using plain HTML (e.g., WordPress).
- **ec-react:** components for sites using React (e.g., GatsbyJS).

Each library has its own directory, which contains a README with more information.
